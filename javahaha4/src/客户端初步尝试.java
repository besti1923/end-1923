
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class 客户端初步尝试 {
    public static void main(String[] args) throws IOException {
        String host = "127.0.0.1";
        int port = 8080;
        //Socket socket = new Socket(host, port);
        //OutputStream output = socket.getOutputStream();



        Scanner scan = new Scanner(System.in);

        String message=new String();



        while(true){
            Socket socket = new Socket(host, port);
            OutputStream output = socket.getOutputStream();
            System.out.println("输入信息");
            message=scan.next();
            output.write(message.getBytes("utf-8"));
            socket.shutdownOutput();

            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String read = input.readLine();
            System.out.println("服务器的内容   " +read);
            input.close();

            if(message.equals("end")) break;
        }






        //message="1/4+1/3";
        //output.write(message.getBytes("utf-8"));




                       //正常来说，客户端打开一个输出流，如果不做约定，也不关闭它，那么服务端永远不知道客户端是否发送完消息，
                       //那么服务端会一直等待下去，直到读取超时。所以怎么告知服务端已经发送完消息就显得特别重要。
        //socket.close();

        //Socket关闭的时候，服务端就会收到响应的关闭信号，那么服务端也就知道流已经关闭了，这个时候读取操作完成，就可以继续后续工作
    }
}
