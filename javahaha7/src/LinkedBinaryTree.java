import javafoundations.*;
import javafoundations.exceptions.*;


import java.util.ArrayList;
import java.util.Arrays;

public class LinkedBinaryTree<T> implements BinaryTree<T>
{
    protected BTNode<T> root;

    public LinkedBinaryTree()
    {
        root = null;
    }


    public LinkedBinaryTree (T element)
    {
        root = new BTNode<T>(element);
    }

    public LinkedBinaryTree (T element, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right) {
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T getRootElement() {
        if (root == null)
            throw new EmptyCollectionException("Get root operation "
                    + "failed. The tree is empty.");

        return root.getElement();
    }

    public LinkedBinaryTree<T> getLeft() {
        if (root == null)
            throw new EmptyCollectionException("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();

        return result;
    }

    public T find (T target) {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new ElementNotFoundException("Find operation failed. "
                    + "No such element in tree.");

        return node.getElement();
    }

    public int size() {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    public ArrayList<T> inorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.inorder (iter);

        return iter;
    }

    public ArrayList<T> levelorder() {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode<T> current = queue.dequeue();

                iter.add (current.getElement());

                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }
        }

        return iter;
    }

    public LinkedBinaryTree<T> getRight() {
        if (root == null)
            throw new EmptyCollectionException("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getRight();

        return result; }

    public boolean contains (T target) {
        if(root.find(target)==null)
            return false;
        else
            return true;
    }

    public boolean isEmpty() {
        if(root!=null)
            return false;
        else
            return true;
    }

    public String toString() {
        return super.toString();
    }

    public ArrayList<T> preorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.preorder (iter);

        return iter;
    }

    public ArrayList<T> postorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.postorder (iter);

        return iter;
    }

    public BTNode construct(char[] pre, char[] in){
        if (pre.length == 0 || in.length == 0) {
            return null;
        }
        BTNode<Character> tree = new BTNode<Character>(pre[0]);
        int index = search(0, in.length, in, tree.getElement());
        tree.setLeft(construct(Arrays.copyOfRange(pre, 1, index + 1), Arrays.copyOfRange(in, 0, index)));
        tree.setRight(construct(Arrays.copyOfRange(pre, index + 1, pre.length),
                Arrays.copyOfRange(in, index + 1, in.length)));
        return tree;
    }

    public int search(int start, int end, char[] inOrders, char data) {
        for (int i = start; i < end; i++) {
            if (data == inOrders[i]) {
                return i;
            }
        }
        return -1;
    }

    public void preOrder(BTNode<T> Tree)
    {
        System.out.print(Tree.getElement()+" ");
        BTNode<T> leftTree = Tree.left;
        if(leftTree != null)
        {
            preOrder(leftTree);
        }
        BTNode<T> rightTree = Tree.right;
        if(rightTree != null)
        {
            preOrder(rightTree);
        }
    }

    public static void inOrder(BTNode tree)
    {
        if(tree == null)
            return;
        else
        {
            inOrder(tree.left);
            System.out.print(tree.print()+" ");
            inOrder(tree.right);
        }
    }
}