import junit.framework.TestCase;

public class LinkedBinaryTreeTest extends TestCase {

    LinkedBinaryTree l1=new LinkedBinaryTree(1);
    LinkedBinaryTree l2=new LinkedBinaryTree(2);
    LinkedBinaryTree l3=new LinkedBinaryTree(3,l1,l2);
    LinkedBinaryTree l4=new LinkedBinaryTree(4);
    LinkedBinaryTree l5=new LinkedBinaryTree(5,l3,l4);


    public void testGetRootElement() {
    }

    public void testGetLeft() {
        assertEquals(1,l3.getLeft().root.element);
        assertEquals(3,l5.getLeft().root.element);
    }

    public void testGetRight() {
        assertEquals(2,l3.getRight().root.element);
        assertEquals(4,l5.getRight().root.element);
    }

    public void testContains() {
        assertEquals(true,l1.contains(1));
        assertEquals(false,l1.contains(2));
        assertEquals(true,l2.contains(2));
        assertEquals(false,l2.contains(1));
        assertEquals(true,l5.contains(1));
        assertEquals(true,l5.contains(2));
        assertEquals(true,l5.contains(3));
        assertEquals(true,l5.contains(4));
        assertEquals(true,l5.contains(5));

    }

    public void testFind() {
        assertEquals("[5, 3, 4, 1, 2]",l5.levelorder().toString());
        assertEquals("[1]",l1.levelorder().toString());
        assertEquals("[2]",l2.levelorder().toString());
        assertEquals("[3, 1, 2]",l3.levelorder().toString());
    }

    public void testSize() {
        assertEquals(1,l1.size());
        assertEquals(1,l2.size());
        assertEquals(3,l3.size());
        assertEquals(1,l4.size());
        assertEquals(5,l5.size());
    }

    public void testInorder() {
        assertEquals("[1]",l1.inorder().toString());
        assertEquals("[2]",l2.inorder().toString());
        assertEquals("[1, 3, 2]",l3.inorder().toString());
        assertEquals("[4]",l4.inorder().toString());
        assertEquals("[1, 3, 2, 5, 4]",l5.inorder().toString());
    }

    public void testIsEmpty() {
        assertEquals(false,l1.isEmpty());
        assertEquals(false,l2.isEmpty());
        assertEquals(false,l3.isEmpty());
        assertEquals(false,l4.isEmpty());
        assertEquals(false,l5.isEmpty());
    }


    public void testPreorder() {
        assertEquals("[1]",l1.preorder().toString());
        assertEquals("[2]",l2.preorder().toString());
        assertEquals("[3, 1, 2]",l3.preorder().toString());
        assertEquals("[4]",l4.preorder().toString());
        assertEquals("[5, 1, 3, 2, 4]",l5.preorder().toString());

    }

    public void testPostorder() {
        assertEquals("[1]",l1.postorder().toString());
        assertEquals("[2]",l2.postorder().toString());
        assertEquals("[1, 2, 3]",l3.postorder().toString());
        assertEquals("[4]",l4.postorder().toString());
        assertEquals("[1, 3, 2, 4, 5]",l5.postorder().toString());
    }
}