public class part2 {
    public static void main(String[] args) {
        LinkedBinaryTree t = new LinkedBinaryTree();
        BTNode tree;
        char[] pre = {'A','B','D','H','I','E','J','M','N','C','F','G','K','L'};
        char[] in = {'H','D','I','B','E','M','J','N','A','F','C','K','G','L'};
        tree = t.construct(pre,in);

        System.out.println("先序遍历");
        t.preOrder(tree);
        System.out.println("\n中序遍历");
        t.inOrder(tree);
    }
}