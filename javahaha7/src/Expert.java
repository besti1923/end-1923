
import java.util.Scanner;

public class Expert {
    private LinkedBinaryTree<String> tree;

    public Expert(){
        String e1 = "喜欢球鞋？";
        String e2 = "有钱？";
        String e3 = "那没事了";
        String e4 = "你觉得李宁球鞋好吗";
        String e5 = "没钱你说个锤子";
        String e6 = "那你去买李宁去吧";
        String e7 = "就是，李宁脸都不要了，四百块一双鞋能炒到一千多是真的离谱";

        LinkedBinaryTree<String> n2,n3,n4,n5,n6,n7;
        n3 = new LinkedBinaryTree<String>(e3);
        n5 = new LinkedBinaryTree<String>(e5);
        n6 = new LinkedBinaryTree<String>(e6);
        n7 = new LinkedBinaryTree<String>(e7);
        n4 = new LinkedBinaryTree<String>(e4,n6,n7);
        n2 = new LinkedBinaryTree<String>(e2,n4,n5);

        tree = new LinkedBinaryTree<String>(e1,n2,n3);
    }

    public void diagnose(){
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;

        System.out.println("开始");
        while(current.size()>1)
        {
            System.out.println(current.getRootElement());
            if(scan.nextLine().equalsIgnoreCase("Y"))
                current = current.getLeft();
            else
                current = current.getRight();
        }
        System.out.println(current.getRootElement());
    }
}
