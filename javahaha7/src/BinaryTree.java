import java.util.ArrayList;

public interface BinaryTree<T>
{
    public T getRootElement();
    public BinaryTree<T> getLeft();
    public BinaryTree<T> getRight();
    public boolean contains(T target);
    public T find(T target);
    public boolean isEmpty();
    public int size();
    public String toString();
    public ArrayList<T> preorder();
    public ArrayList<T> inorder();
    public ArrayList<T> postorder();
    public ArrayList<T> levelorder();
}