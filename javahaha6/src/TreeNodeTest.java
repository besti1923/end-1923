
public class TreeNodeTest {
    public static void main(String[] args) {
        TreeNode treeNode=new TreeNode();
        treeNode.insert(39);
        treeNode.insert(24);
        treeNode.insert(64);
        treeNode.insert(23);
        treeNode.insert(30);
        treeNode.insert(53);
        treeNode.insert(60);
        //前序遍历
        System.out.println("前序遍历");
        treeNode.preOrder(treeNode.root);
        System.out.println();
        //中序遍历
        System.out.println("中序遍历");
        treeNode.inOrder(treeNode.root);
        System.out.println();
        //后续遍历
        System.out.println("后序遍历");
        treeNode.postOrder(treeNode.root);
        System.out.println();
        //层序遍历
        System.out.println("层序遍历");
        treeNode.levelOrder(treeNode.root);
        System.out.println();

        /*System.out.println(treeNode.find(64));*/
        try {
            System.out.println(treeNode.otherfind(24));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(treeNode.leaf(treeNode.root));
        //System.out.println(treeNode.otherleaf(treeNode.root,0));
    }
}