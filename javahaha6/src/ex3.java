public class ex3 {

    public static class 对分查找{
        public static int 查找 (int[] arr ,int a) {
            int n=arr.length;
            int i=0;int mid;
            int j=n-1;

            while(i<=j)
            {
                mid = (i+j)/2;
                if(arr[mid]==a)
                    return mid;
                if(arr[mid]>a)
                    j = mid-1;
                if(arr[mid]<a)
                    i = mid+1;
            }
            return -1;
        }
    }



    public static class 插入查找{
        public static int 查找(int a[], int value, int low, int high)
        {
            int mid = low+(value-a[low])/(a[high]-a[low])*(high-low);
            if(a[mid]==value)
                return mid;
            else if(a[mid]>value)
                return 查找(a, value, low, mid-1);
                //if(a[mid]<value)
            else
                return 查找(a, value, mid+1, high);
        }
    }








    public static class FibonacciSearch {
        /*
         * 斐波那契数列
         * 采用递归
         */
        public static int fib(int n) {
            if(n==0)
                return 0;
            if(n==1)
                return 1;
            return fib(n-1)+fib(n-2);
        }

        /*
         * 斐波那契数列
         * 不采用递归
         */
        public static int fib2(int n) {
            int a=0;
            int b=1;
            if(n==0)
                return a;
            if(n==1)
                return b;
            int c=0;
            for(int i=2;i<=n;i++) {
                c=a+b;
                a=b;
                b=c;
            }
            return c;
        }

        /*
         * 斐波那契查找
         */
        public static int fibSearch(int[] arr,int n,int key) {
            int low=1;  //记录从1开始
            int high=n;     //high不用等于fib(k)-1，效果相同
            int mid;

            int k=0;
            while(n>fib(k)-1)    //获取k值
                k++;
            int[] temp = new int[fib(k)];   //因为无法直接对原数组arr[]增加长度，所以定义一个新的数组
            System.arraycopy(arr, 0, temp, 0, arr.length); //采用System.arraycopy()进行数组间的赋值
            for(int i=n+1;i<=fib(k)-1;i++)    //对数组中新增的位置进行赋值
                temp[i]=temp[n];

            while(low<=high) {
                mid=low+fib(k-1)-1;
                if(temp[mid]>key) {
                    high=mid-1;
                    k=k-1;  //对应上图中的左段，长度F[k-1]-1
                }else if(temp[mid]<key) {
                    low=mid+1;
                    k=k-2;  //对应上图中的右端，长度F[k-2]-1
                }else {
                    if(mid<=n)
                        return mid;
                    else
                        return n;       //当mid位于新增的数组中时，返回n
                }
            }
            return -1;
        }
    }


    public static class ShellSort {

        public  static void Sort(int[] arr) {
            //step:步长
            for (int step = arr.length / 2; step > 0; step /= 2) {
                //对一个步长区间进行比较 [step,arr.length)
                for (int i = step; i < arr.length; i++) {
                    int value = arr[i];
                    int j;

                    //对步长区间中具体的元素进行比较
                    for (j = i - step; j >= 0 && arr[j] > value; j -= step) {
                        //j为左区间的取值，j+step为右区间与左区间的对应值。
                        arr[j + step] = arr[j];
                    }
                    //此时step为一个负数，[j + step]为左区间上的初始交换值
                    arr[j + step] = value;
                }
            }
        }
    }







}
