

public interface minestack<T> {
    //压栈
    public void push(T element);
    //弹栈
    public T pop();
    //展示栈顶元素
    public T peek();
    //判断是否栈空
    public boolean isEmpty();
    //反馈栈长
    public int size();
    //显示栈
    public String toString();
}