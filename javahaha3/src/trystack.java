

    import java.util.Arrays;

    public class trystack<T> implements minestack<T> {
        private int DEFINE_LONG = 100;
        private int top;
        private T[] stack;
        //构造函数一（用户不指定栈长）
        public trystack() {
            top = 0;
            stack = (T[])(new Object[DEFINE_LONG]);

        }
        //构造函数二（用户指定栈长）
        public trystack(int initialCapacity) {
            top = 0;
            stack = (T[])(new Object[initialCapacity]);
        }

        //压栈方法
        @Override
        public void push(T element) {
            //若栈满调用方法增加长度
            if(size() == stack.length)
                expandCapacity();
            stack[top] = element;
            top++;
        }
        //数组增长方法
        private void expandCapacity() {
            stack = Arrays.copyOf(stack, stack.length*2);
        }

        //弹栈方法
        @Override
        public T pop() throws EmptyCollectionException{
            if(isEmpty())
                throw new EmptyCollectionException("stack");
            top--;
            T temp = stack[top];
            stack[top] = null;
            return temp;
        }

        //查看栈顶元素方法
        @Override
        public T peek() {
            if(isEmpty())
                throw new EmptyCollectionException("stack");
            return stack[top-1];
        }

        //判断栈空方法
        @Override
        public boolean isEmpty() {
            if(stack[top] == null && top == 0)
                return true;
            else
                return false;
        }

        //查看栈元素长度方法
        @Override
        public int size() {
            return top;
        }
    }
    //新异常定义
    class EmptyCollectionException extends RuntimeException{
        public EmptyCollectionException(String collection) {
            super("The "+ collection +" is empty");
        }
    }

