import junit.framework.TestCase;
import org.junit.Test;

public class util2Test extends TestCase {
    @Test
    public void testNormal() {
        assertEquals("不及格", util2.percentage2fivegrade(55));
        assertEquals("及格", util2.percentage2fivegrade(65));
        assertEquals("中等", util2.percentage2fivegrade(75));
        assertEquals("良好", util2.percentage2fivegrade(85));
        assertEquals("优秀", util2.percentage2fivegrade(95));
    }

    public void testex(){
        assertEquals("错误", util2.percentage2fivegrade(-55));
        assertEquals("错误", util2.percentage2fivegrade(105));

    }

    public void testboundary(){
        assertEquals("不及格", util2.percentage2fivegrade(0));
        assertEquals("及格", util2.percentage2fivegrade(60));
        assertEquals("中等", util2.percentage2fivegrade(70));
        assertEquals("良好", util2.percentage2fivegrade(80));
        assertEquals("优秀", util2.percentage2fivegrade(90));

    }


}