import junit.framework.TestCase;


public class StringBuffferDemoTest extends TestCase {

    StringBuffer a = new StringBuffer("StringBUffer");//测试12个字符
    StringBuffer b = new StringBuffer("StringBufferStringBUffer");//test 24 character
    StringBuffer c = new StringBuffer("StringBufferStringBufferStringBUffer");//test 36 character

    public void testcharAt() throws  Exception{
        assertEquals('S',a.charAt(0));
        assertEquals('g',a.charAt(5));
        assertEquals('r',a.charAt(11));
    }

    public void testcapacity() throws Exception{
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(52,c.capacity());
    }

    public void testlength() throws Exception{
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(36,c.length());
    }

    public void testindexof() throws Exception{
        assertEquals(0,a.indexOf("Str"));
        assertEquals(5,a.indexOf("gBU"));

    }



}