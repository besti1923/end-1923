import junit.framework.TestCase;
import org.junit.Test;

public class mycomplexTest extends TestCase {
    mycomplex a = new mycomplex(1,1);
    mycomplex b = new mycomplex(2,2);
    @Test
    public void testcomplexadd() throws Exception{
       assertEquals(2.0, a.complexadd(a).getrealpart());
       assertEquals(2.0, a.complexadd(a).getimagepart());
    }

    public void testcomplexmin() throws Exception{
        assertEquals(1.0,b.complexmin(a).getrealpart());
        assertEquals(1.0,b.complexmin(a).getimagepart());
    }

    public void testcomplexmul() throws Exception{
         assertEquals(0.0,b.complexmul(b).getrealpart());
         assertEquals(8.0,b.complexmul(b).getimagepart());
    }

    public void testcomplexdiv() throws Exception{
        assertEquals(1.0,b.complexdiv(b).getrealpart());
        assertEquals(0.0,b.complexdiv(b).getimagepart());
    }

}