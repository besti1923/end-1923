
abstract class Data {
    abstract public void DisplayValue();
}


class myshort extends  Data{
    short value;
    myshort(){value=69;}

    public void DisplayValue(){
        System.out.println(value);
    }
}


class Integer extends Data {
    int value;
    Integer() {
        value=100;
    }

    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes


abstract class Factory {
    abstract public Data CreateDataObject();
}


class shortFactory extends Factory{
    public Data CreateDataObject(){
        return new myshort();
    }
}


class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}



class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}



public class MyDoc {
    static Document d;
    static Document e;
    public static void main(String[] args) {
        System.out.println("8%6=2,2---------short");

        e = new Document(new shortFactory());
        e.DisplayData();

        d = new Document(new IntFactory());
        d.DisplayData();


    }
}