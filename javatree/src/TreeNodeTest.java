
public class TreeNodeTest {
    public static void main(String[] args) {
        TreeNode treeNode=new TreeNode();
        treeNode.insert(24);treeNode.insert(55);
        treeNode.insert(36);treeNode.insert(15);
        treeNode.insert(30);treeNode.insert(41);
        treeNode.insert(60);
        System.out.println("前序遍历");
        treeNode.preOrder(treeNode.root);
        System.out.println();
        System.out.println("中序遍历");
        treeNode.inOrder(treeNode.root);
        System.out.println();
        System.out.println("后序遍历");
        treeNode.postOrder(treeNode.root);
        System.out.println();
        System.out.println("层序遍历");
        treeNode.levelOrder(treeNode.root);
        System.out.println();
        try {
            System.out.println(treeNode.find(24));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(treeNode.leaf(treeNode.root));

    }
}