
import javax.swing.text.Element;
import java.net.Socket;
import java.util.LinkedList;

public class TreeNode<T extends Comparable<T>> {



    Node<T> root;//根节点
    public void insert(T key)
    {
        Node p=new Node();//待插入的新的节点
        p.data=key;
        //如果树是空的
        if (root==null) {
            root=p;
            return;
        }


            Node current = root;
            while (true) {
                if ((key.compareTo((T) current.data)) > 0) /*比现在的节点大*/{
                    if(current.right == null) /*比现在的节点大并且右边为空*/{
                        current.right = p;
                        break;
                    } else /*右边不为空*/{
                        current = current.right;
                        continue;
                    }

                } else
                {
                    if(current.left == null) {
                        current.left = p;
                        break;
                    } else {
                        current = current.left;
                        continue;
                    }
                }

            }
    }
    public void preOrder(Node root)
    { // 前序遍历,"中左右"
        if (root != null)
        {
            System.out.print(root.data + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }
    public void inOrder(Node root)
    { // 中序遍历,"左中右"
        if (root != null)
        {
            inOrder(root.left);
            System.out.print(root.data + " ");
            inOrder(root.right);
        }
    }
    public void postOrder(Node root)
    { // 后序遍历,"左右中"
        if (root != null)
        {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.data + " ");
        }
    }
    public void levelOrder(Node root) {
        Node<T> node =root;
        LinkedList<Node<T>> list = new LinkedList();
        list.add(node);
        while(!list.isEmpty()) {
            node=list.poll();
            System.out.print(node.data+" ");
            if(node.left!=null)
                list.offer(node.left);
            if(node.right!=null)
                list.offer(node.right);
        }
    }

    //节点个数
    public int size(Node root)
    {
        int count=0;
        while (root!=null)
        {
            count++;


        }
        return count;
    }


    public T find(T targetElement)throws Exception
    {
        Node<T> current=findAgain(targetElement,root);
        if (current==null)
            throw new Exception("not find in this tree");
        return (current.data);
    }

    private Node<T> findAgain(T targetElement,Node<T> next)
    {
        if (next==null)
            return null;
        if (next.data.equals(targetElement))
            return next;
        Node<T> temp=findAgain(targetElement,next.left);
        if (temp==null)
            temp=findAgain(targetElement,next.right);
        return temp;

    }
    //叶子节点数
    public int leaf(Node p) {
        if (p == null)
            return 0;
        if (p.right == null && p.left == null)
            return 1;
        return leaf(p.right) + leaf(p.left);


    }

}







