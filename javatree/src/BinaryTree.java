
import java.util.Iterator;

public interface BinaryTree<T> extends Iterable<T> {

    public T getrootElement();

    public BinaryTree<T> getleft();

    public BinaryTree<T> getright();

    public boolean contains(T target);

    public T find(T target);

    public boolean isEmpty();

    public int size ();

    public String tostring();

    public Iterator<T> inorder();

    public Iterable<T> preorder();

    public Iterable<T> postorder();

    public Iterable<T> levelorder();
}
