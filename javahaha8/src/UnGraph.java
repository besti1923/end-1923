import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class UnGraph {
    private String[] vexs = null;
    private int[][] arcs = null;
    private boolean[] visited = null;
    public UnGraph(int vexNum,int arcNum) {

        this.vexs = new String[vexNum];
        this.arcs = new int[vexNum][vexNum];

        System.out.print("请依次输入顶点值，以空格隔开：");
        Scanner sc = new Scanner(System.in);
        for(int i = 0; i < vexNum; i++) {
            this.vexs[i] = sc.next();
        }

        for(int i = 0; i < vexNum; i++) {
            for(int j = 0; j < vexNum; j++) {
                this.arcs[i][j] = 0;
            }
        }

        start:for(int i = 0; i < arcNum; i++) {
            sc = new Scanner(System.in);
            int vex1Site = 0;
            int vex2Site = 0;
            String vex1 = null;
            String vex2 = null;

            System.out.print("请输入第" + (i+1) + "条边所依附的两个顶点，以空格隔开：");
            vex1 = sc.next();
            vex2 = sc.next();
            for(int j = 0; j < this.vexs.length; j++) {
                if (this.vexs[j].equals(vex1)) {
                    vex1Site = j;
                    break;
                }
                if (j == this.vexs.length - 1) {
                    System.out.println("未找到第一个顶点，请重新输入！");
                    i--;
                    continue start;
                }
            }
            for (int j = 0; j < this.vexs.length; j++) {
                if(this.vexs[j].equals(vex2)) {
                    vex2Site = j;
                    break;
                }
                if (j == this.vexs.length - 1) {
                    System.out.println("未找到第二个顶点，请重新输入！");
                    i--;
                    continue start;
                }
            }
            if(this.arcs[vex1Site][vex2Site] != 0) {
                System.out.println("该边已存在！");
                i--;
                continue start;
            }else {
                this.arcs[vex1Site][vex2Site] = 1;
                this.arcs[vex2Site][vex1Site] = 1;
            }
        }
        System.out.println("基于邻接矩阵的无向图创建成功！");
        sc.close();
    }

    public void dFSTraverse() {
        this.visited = new boolean[this.vexs.length];
        for(int i = 0; i < this.visited.length; i++) {
            this.visited[i] = false;
        }

        for(int i = 0; i < this.visited.length; i++) {
            if(!this.visited[i]) {
                dFS_AM(i);
            }
        }
    }
    public void dFS_AM(int site) {
        System.out.println(this.vexs[site]);
        this.visited[site] = true;
        for(int i = 0; i < this.vexs.length; i++) {
            if(this.arcs[site][i] != 0 && !this.visited[i]) {
                this.dFS_AM(i);
            }
        }
    }
    public void bFSTraverse() {

        this.visited = new boolean[this.vexs.length];
        for(int i = 0; i < this.visited.length; i++) {
            this.visited[i] = false;
        }

        for(int i = 0; i < this.visited.length; i++) {
            if(!this.visited[i]) {
                bFS_AM(i);
            }
        }
    }
    public void bFS_AM(int site) {
        System.out.println(this.vexs[site]);
        this.visited[site] = true;
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        linkedList.offer(site);
        while(!linkedList.isEmpty()) {
            int vexSite = linkedList.poll();
            for(int i = 0; i < this.vexs.length; i++) {
                if(this.arcs[vexSite][i] != 0 && !this.visited[i]) {
                    System.out.println(this.vexs[i]);
                    this.visited[i] = true;
                    linkedList.offer(i);
                }
            }
        }
    }
}
