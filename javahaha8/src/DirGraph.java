import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Stack;
public class DirGraph {
    static class DirectedDFS {
        private boolean[] marked;
        private Dirgraph G;
        private Stack<Integer> cycle;
        private int []edgeTo;
        private boolean[] onStack;
        private Stack<Integer> reversePost;

        public DirectedDFS(Dirgraph G) {
            marked = new boolean[G.V()];
            edgeTo = new int[G.V()];
            this.G = G;
            DirectedCycle();
            DepthFirstOrder();
        }

        public void dfs(int s) {
            marked[s] = true;
            for (int w : G.adj(s)) {
                if (!marked[w]) {
                    edgeTo[w] = s;
                    dfs(w);
                }
            }
        }

        private void bfs(int s) {
            ArrayDeque<Integer> deque = new ArrayDeque<Integer>();
            deque.add(s);
            marked[s] = true;
            int c;
            while (!deque.isEmpty()) {
                c = deque.poll();
                for (int w : G.adj(c)) {
                    if (!marked[w]) {
                        marked[w] = true;
                        edgeTo[w] = c;
                        deque.add(w);
                    }
                }
            }
        }

        public void DFS(int s) {
            dfs(s);
        }

        public void DFS(Iterable<Integer> sources) {
            for (Integer integer : sources) {
                dfs(integer);
            }
        }

        public boolean marked(int v) {
            return marked[v];
        }

        public void DFDPath(int s, int c) {
            for (int i = 0; i < marked.length; i++) {
                marked[i] = false;
            }
            dfs(s);
            System.out.print(c + "<-");
            while (edgeTo[c] != s) {
                c = edgeTo[c];
                System.out.print(c + "<-");
            }
            c = edgeTo[c];
            System.out.println(c);
        }

        public void BFDPath(int s, int c) {
            for (int i = 0; i < marked.length; i++) {
                marked[i] = false;
            }
            bfs(s);
            System.out.print(c + "<-");
            while (edgeTo[c] != s) {
                c = edgeTo[c];
                System.out.print(c + "<-");
            }
            c = edgeTo[c];
            System.out.println(c);
        }

        private void DirectedCycle() {
            onStack = new boolean[G.V()];
            for (int i = 0; i < marked.length; i++) {
                marked[i] = false;
            }
            for (int i = 0; i < G.V(); i++) {
                if (!marked[i]) DirectedCycleDfs(i);
            }
        }

        private void DirectedCycleDfs(int s) {
            onStack[s] = true;
            marked[s] = true;
            for (int w : G.adj(s)) {
                if (this.hasCycle()) return;
                else if (!marked[w]) {
                    edgeTo[w] = s;
                    DirectedCycleDfs(w);
                }
                else if (onStack[w]) {
                    cycle = new Stack<Integer>();
                    for (int x = s; x != w; x = edgeTo[x])
                        cycle.push(x);

                    cycle.push(w);
                    cycle.push(s);
                }
            }
            onStack[s] = false;
        }
        public boolean hasCycle()
        {
            return cycle != null;
        }
        public Iterable<Integer> cycle()
        {
            return cycle;
        }

        public void DepthFirstOrder() {
            reversePost = new Stack<Integer>();
            for (int i = 0; i < marked.length; i++) {
                marked[i] = false;
            }
            for (int i = 0; i < G.V(); i++) {
                if (!marked[i]) dfo(i);
            }
        }
        private void dfo(int s) {
            marked[s] = true;
            for (int w : G.adj(s)) {
                if (!marked[w]) {
                    dfo(w);
                }
            }
            reversePost.push(s);
        }

        public Iterable<Integer> Topological() {
            if (hasCycle()) { return null;}
            return reversePost;
        }
    }

    static class Dirgraph {
        private Bag<Integer>[] digraph;
        private int V;
        private int E;
        public Dirgraph(int V) {
            this.V = V;
            digraph = (Bag<Integer>[]) new Bag[V];
            for (int i = 0; i < V; i++) {
                digraph[i] = new Bag<Integer>();
            }
        }

        public int V() { return V;}
        public int E() { return E;}

        public void addEdge(int v, int w) {
            digraph[v].add(w);
            E++;
        }

        public Iterable<Integer> adj(int V) {
            return digraph[V];
        }

        public Dirgraph reverse() {
            Dirgraph result = new Dirgraph(V);
            for (int i = 0; i < V; i++) {
                for (Integer integer : digraph[i]) {
                    result.addEdge(i, integer);
                }
            }
            return result;
        }

        public String toString() {
            String s = V + " vertex, " + E + " edges\n";
            for (int v = 0; v < V; v++) {
                s += v + ": ";
                for (Integer integer : this.adj(v)) {
                    s += integer + " ";
                }
                s += "\n";
            }
            return s;
        }
    }

    static class Bag<T> implements Iterable<T> {
        Node first;

        private class Node {
            T value;
            Node next;
        }

        public void add(T value) {
            Node oldfirst = first;
            first = new Node();
            first.value = value;
            first.next = oldfirst;
        }

        @Override
        public Iterator<T> iterator() {
            return new BagIterator();
        }

        private class BagIterator implements Iterator<T> {
            Node node = first;

            @Override
            public boolean hasNext() {
                return node != null;
            }

            @Override
            public T next() {
                T tempt = node.value;
                node = node.next;
                return tempt;
            }
        }
    }
}
