import java.sql.SQLOutput;
import java.util.Scanner;

public class Realize {
    public static void main(String[] args) {
        System.out.println("无向图（1） Or 有向图（2）？");
        Scanner scan=new Scanner(System.in);
        int Direct =scan.nextInt();
        if(Direct==1)
        {
            int vexNum = 0;
            int arcNum = 0;
            while(true) {
                System.out.print("请输入要建立无向图的总顶点数和总边数，以空格隔开：");
                try {
                    vexNum = scan.nextInt();
                    arcNum = scan.nextInt();
                    break;
                } catch (Exception e) {
                    System.out.println("输入不合法！");
                    continue;
                }
            }

            UnGraph aMGraph = new UnGraph(vexNum, arcNum);
            System.out.println("由深度优先遍历得：");
            aMGraph.dFSTraverse();
            System.out.println("由广度优先遍历得：");
            aMGraph.bFSTraverse();

            scan.close();
        }
        else if(Direct==2)
        {
            DirGraph.Dirgraph a = new DirGraph.Dirgraph(6);
            a.addEdge(0, 1);
            a.addEdge(0, 3);
            a.addEdge(1, 2);
            a.addEdge(2, 5);
            a.addEdge(3, 2);
            a.addEdge(4, 3);
            a.addEdge(4, 2);
            System.out.println(a);

            DirGraph.DirectedDFS dfsa = new DirGraph.DirectedDFS(a);
            System.out.println(dfsa.hasCycle());
            System.out.println();


            DirGraph.Dirgraph b = new DirGraph.Dirgraph(6);
            a.addEdge(0, 1);
            a.addEdge(0, 3);
            a.addEdge(1, 2);
            a.addEdge(2, 5);
            a.addEdge(3, 2);
            a.addEdge(4, 3);
            a.addEdge(4, 2);
            DirGraph.DirectedDFS dfsb = new DirGraph.DirectedDFS(b);
            System.out.println(dfsb.hasCycle());
            System.out.println("由深度优先遍历得：");
            for (Integer integer : dfsb.Topological()) {
                System.out.print(integer + " ");
            }
            System.out.println("由广度优先遍历得：");
            DirGraph.DirectedDFS bfs=new DirGraph.DirectedDFS(b);
        }
    }
}